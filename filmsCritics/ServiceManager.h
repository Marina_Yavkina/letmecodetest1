//
//  ServiceManager.h
//  filmsCritics
//
//  Created by Марина Явкина on 08/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <Foundation/Foundation.h>
#

NS_ASSUME_NONNULL_BEGIN

@interface ServiceManager : NSObject
+ (ServiceManager*) sharedManager;

- (void) getRevieweresWothOffset:(NSInteger) offset
                      andQuery:(NSString*) query
                       andDate:(NSString*) rwDate
                   andRewiewer:(NSString*) reviewer
                      onSuccess:(void(^)(NSDictionary* reviewes)) success
                      onFailure:(void(^)(NSError* error,NSInteger stutusCode)) failure;
-(void) getCriticsWithRequest:(NSString *)query
                    onSuccess:(void(^)(NSDictionary* reviewes)) success
                    onFailure:(void(^)(NSError* error,NSInteger stutusCode)) failure;

@end

NS_ASSUME_NONNULL_END
