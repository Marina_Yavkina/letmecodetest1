//
//  ReviewersViewController.h
//  filmsCritics
//
//  Created by Марина Явкина on 08/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReviewersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate >
{
    UIToolbar* toolbar;
    UIDatePicker* picker;
}
@property (weak, nonatomic) IBOutlet UITextField *CalendarTF;
@property (weak, nonatomic) IBOutlet UITextField *SearchTF;
@property (weak, nonatomic) IBOutlet UITableView *revTableView;
- (IBAction)searchBtn:(id)sender;
- (IBAction)calendarBtn:(id)sender;


@end

NS_ASSUME_NONNULL_END
