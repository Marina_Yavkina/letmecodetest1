//
//  ReviewersViewController.m
//  filmsCritics
//
//  Created by Марина Явкина on 08/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import "ReviewersViewController.h"
#import "ServiceManager.h"
#import "ReviewersTableViewCell.h"

@interface ReviewersViewController ()
@property (strong, nonatomic) NSArray* revieweres;
@property (assign,nonatomic) BOOL isCanLoad;
@property (nonatomic, strong) ReviewersTableViewCell *prototypeCell;
@property (assign,nonatomic) BOOL isSearch;
@property (assign,nonatomic) NSInteger ioffset;

@end

@implementation ReviewersViewController


- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.ioffset = 0;
    self.isSearch = NO;
    self.revieweres = [NSMutableArray array];
     
   
    [self getReviewersFromServerWithOffset:0 andDate:nil andRewiewer:nil andQuery:nil];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
}

- (ReviewersTableViewCell *)prototypeCell {
  if (!_prototypeCell) {
    _prototypeCell = [self.revTableView
     dequeueReusableCellWithIdentifier:@"rwCell"];
  }
  return _prototypeCell;
}

- (IBAction)searchBtn:(id)sender{
    self.isSearch = YES;
    self.revieweres = nil;
    self.revieweres = [NSMutableArray array];
    [self getReviewersFromServerWithOffset:[self.revieweres count] andDate:self.CalendarTF.text andRewiewer:nil andQuery:self.SearchTF.text];
    self.ioffset = 20;
    [self.revTableView reloadData];
}
- (IBAction)calendarBtn:(id)sender{
    picker = [[UIDatePicker alloc] init];
    picker.backgroundColor = [UIColor whiteColor];
    [picker setValue:[UIColor blackColor] forKey:@"textColor"];

    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    picker.datePickerMode = UIDatePickerModeDate;

    [picker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    picker.frame = CGRectMake(0.0, [UIScreen mainScreen].bounds.size.height - 300, [UIScreen mainScreen].bounds.size.width, 300);
    [self.view addSubview:picker];

    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 300, [UIScreen mainScreen].bounds.size.width, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onDoneButtonClick)]];
    [toolbar sizeToFit];
    [self.view addSubview:toolbar];
}


#pragma mark - API
- (void) getReviewersFromServerWithOffset:(NSInteger)offset
                                  andDate:(NSString *)rwDate
                              andRewiewer:(NSString *)reviewer
                                 andQuery:(NSString *)query{
   [[ServiceManager sharedManager] getRevieweresWothOffset:offset//[self.revieweres count]
                                                   andQuery:query
                                                    andDate:rwDate
                                                andRewiewer:reviewer
                                                  onSuccess:^(NSDictionary* _Nonnull reviewes) {
    
        self.isCanLoad = [reviewes objectForKey:@"has_more"];
        NSArray* resp = [reviewes objectForKey:@"results"];
        
        self.revieweres = [[self.revieweres mutableCopy] arrayByAddingObjectsFromArray:resp];
        [self.revTableView reloadData];
    }
                                                  onFailure:^(NSError* _Nonnull error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %d",error.localizedDescription, (int)statusCode);
    }];
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.revieweres count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identifier = @"rwCell";
    ReviewersTableViewCell *cell = (ReviewersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    

    NSDictionary* rev = [self.revieweres objectAtIndex:indexPath.row];
    cell.filmTitle.text = [NSString stringWithFormat:@"%@",[rev objectForKey:@"display_title"]];
    cell.rwDescription.text = [NSString stringWithFormat:@"%@",[rev objectForKey:@"summary_short"]];
    cell.byline.text = [NSString stringWithFormat:@"%@",[rev objectForKey:@"byline"]];
    
    cell.recDate.text = [self ReturnFormattedDateFromString:[NSString stringWithFormat:@"%@",[rev objectForKey:@"date_updated"]]];
    NSDictionary* multimedia = [rev objectForKey:@"multimedia"];
    if (![multimedia isKindOfClass:[NSNull class]])
    {
        NSString * url_str = [NSString stringWithFormat:@"%@",[multimedia objectForKey:@"src"]];
        NSURL *url = [NSURL URLWithString:url_str];
          // @"byline"
         NSData *data = [NSData dataWithContentsOfURL:url];
        CGSize c=CGSizeMake(100, 100);
        cell.imgView.image = [self image:[UIImage imageWithData:data] scaledToSize:c] ;
    }
    //cell.imgView.image = [UIImage imageWithData:data];
    return cell;
}

-(NSString*)ReturnFormattedDateFromString:(NSString*)date{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    NSLog(@"Picked the date %@", [dateFormatter stringFromDate:dateFromString] )  ;
    return [dateFormatter stringFromDate:dateFromString];
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }

    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);

    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];

    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    //return image
    return image;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger offs = 0;
    if (self.isCanLoad)
    {
        if (self.isSearch)
        {
            offs = self.ioffset;
        }
        else
        {
            offs = [self.revieweres count];
        }
        
        [self getReviewersFromServerWithOffset:offs andDate:self.CalendarTF.text andRewiewer:nil andQuery:self.SearchTF.text];
        self.ioffset = self.ioffset + 20;
    }
    else{
        self.isSearch = NO;
    }
}
 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    [self.prototypeCell layoutIfNeeded];
CGSize maximumLabelSize = CGSizeMake(310, 9999);
    CGRect titleRect = [self.prototypeCell.filmTitle.text boundingRectWithSize:maximumLabelSize
    options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    attributes:@{NSFontAttributeName:self.prototypeCell.filmTitle.font}
    context:nil];
    CGRect dateRect = [self.prototypeCell.recDate.text boundingRectWithSize:maximumLabelSize
    options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    attributes:@{NSFontAttributeName:self.prototypeCell.recDate.font}
    context:nil];
    CGRect descRect = [self.prototypeCell.rwDescription.text boundingRectWithSize:maximumLabelSize
    options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    attributes:@{NSFontAttributeName:self.prototypeCell.rwDescription.font}
    context:nil];
    
    CGRect byline = [self.prototypeCell.byline.text boundingRectWithSize:maximumLabelSize
       options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
       attributes:@{NSFontAttributeName:self.prototypeCell.byline.font}
       context:nil];
    return titleRect.size.height + descRect.size.height + dateRect.size.height + byline.size.height + 86;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}




- (void)configureCell:(UITableViewCell *)cell
  forRowAtIndexPath:(NSIndexPath *)indexPath {
  if ([cell isKindOfClass:[ReviewersTableViewCell class]])
  {
    ReviewersTableViewCell *textCell = (ReviewersTableViewCell *)cell;
    textCell.filmTitle.text = [NSString stringWithFormat:@"%@",[[self.revieweres
    objectAtIndex:indexPath.row] objectForKey:@"display_title"]];;
    
    textCell.filmTitle.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    textCell.rwDescription.text = [NSString stringWithFormat:@"%@",[[self.revieweres
    objectAtIndex:indexPath.row] objectForKey:@"summary_short"]];;
    textCell.rwDescription.font = [UIFont fontWithName:@"Helvetica" size:10.0];
    textCell.recDate.text = [NSString stringWithFormat:@"%@",[[self.revieweres
    objectAtIndex:indexPath.row] objectForKey:@"date_updated"]];;
    textCell.rwDescription.font = [UIFont fontWithName:@"Helvetica" size:12.0];
      textCell.byline.text = [NSString stringWithFormat:@"%@",[[self.revieweres
      objectAtIndex:indexPath.row] objectForKey:@"byline"]];;
      textCell.byline.font = [UIFont fontWithName:@"Helvetica" size:12.0];
      
  }
}

-(void) dueDateChanged:(UIDatePicker *)sender {

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSLog(@"Picked the date %@", [dateFormatter stringFromDate:[sender date]]);
    self.CalendarTF.text = [dateFormatter stringFromDate:[sender date]];
    
}
 
-(void)onDoneButtonClick {
    __strong typeof (self) welf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [welf->toolbar removeFromSuperview];
        [welf->picker removeFromSuperview];
    });
   
    self.revieweres = nil;
    self.revieweres = [NSMutableArray array];
    [self getReviewersFromServerWithOffset:[self.revieweres count] andDate:self.CalendarTF.text andRewiewer:nil andQuery:self.SearchTF.text];
    [self.revTableView reloadData];
   
}

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
