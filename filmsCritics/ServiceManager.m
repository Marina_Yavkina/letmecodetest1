//
//  ServiceManager.m
//  filmsCritics
//
//  Created by Марина Явкина on 08/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import "ServiceManager.h"
#import "AFNetworking.h"

@implementation ServiceManager

+ (ServiceManager*) sharedManager{
    static ServiceManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServiceManager alloc] init];
    });
    return manager;
}

-(void) getCriticsWithRequest:(NSString *)query
onSuccess:(void(^)(NSDictionary* reviewes)) success
onFailure:(void(^)(NSError* error,NSInteger stutusCode)) failure
{
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSString * str  = @"https://api.nytimes.com/svc/movies/v2/critics/%@.json?api-key=tzAGtuAkHoiDI4ngvQ0BWTGBGoI70w3o";//&api-key=tzAGtuAkHoiDI4ngvQ0BWTGBGoI70w3o
        
        NSString *strURL = [NSString stringWithFormat:str, query];
        [manager GET:strURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary* responseObject) {
               NSLog(@"JSON:, %@", responseObject);
               if (success) {
                   success(responseObject);
               }
           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               NSLog(error.domain.capitalizedString);
           }];
}

- (void) getRevieweresWothOffset:(NSInteger) offset
   andQuery:(NSString*) query
    andDate:(NSString*) rwDate
andRewiewer:(NSString*) reviewer
  onSuccess:(void(^)(NSDictionary* reviewes)) success
  onFailure:(void(^)(NSError* error,NSInteger stutusCode)) failure{
        
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString * str  = @"https://api.nytimes.com/svc/movies/v2/reviews/search.json?offset=%d";//&api-key=tzAGtuAkHoiDI4ngvQ0BWTGBGoI70w3o
    if  (query && !([[query stringByTrimmingCharactersInSet:
    [NSCharacterSet whitespaceCharacterSet]] length] == 0))
    {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"&query=%@",query]];
    }
    if  (reviewer && !([[reviewer stringByTrimmingCharactersInSet:
    [NSCharacterSet whitespaceCharacterSet]] length] == 0))
    {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"&reviewer=%@",reviewer]];
    }
    if  (rwDate  && !([[rwDate stringByTrimmingCharactersInSet:
    [NSCharacterSet whitespaceCharacterSet]] length] == 0))
    {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"&opening-date=%@",rwDate]];
    }
    NSString *strURL = [NSString stringWithFormat:str, (int)offset];
    strURL = [strURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    strURL = [strURL stringByAppendingString:@"&api-key=tzAGtuAkHoiDI4ngvQ0BWTGBGoI70w3o"];
       [manager GET:strURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary* responseObject) {
           NSLog(@"JSON:, %@", responseObject);
           if (success) {
               success(responseObject);
           }
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           NSLog(error.domain.capitalizedString);
       }];
    
}

@end
