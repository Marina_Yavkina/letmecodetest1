//
//  CriticsViewCell.h
//  filmsCritics
//
//  Created by Марина Явкина on 13/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CriticsViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *criticImage;
@property (weak, nonatomic) IBOutlet UILabel *criticName;

@end

NS_ASSUME_NONNULL_END
