//
//  DetailViewController.m
//  filmsCritics
//
//  Created by Марина Явкина on 13/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import "DetailViewController.h"
#import "ServiceManager.h"
#import "ReviewersTableViewCell.h"

@interface DetailViewController ()
@property (strong, nonatomic) NSArray* reviewes;
@property (assign,nonatomic) BOOL isCanLoad;
@property (nonatomic, strong) ReviewersTableViewCell *prototypeCell;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameCr.text = self.selectedName;
    self.descr.text = self.selectedBio;
    self.status.text = self.selectedStatus;
    self.imageCr.image = self.selectedImage;
    self.reviewes = [NSMutableArray array];
    [self getReviewersFromServerWithOffset:0 andDate:nil andRewiewer:self.selectedName andQuery:nil];
    // Do any additional setup after loading the view.
}

- (ReviewersTableViewCell *)prototypeCell {
  if (!_prototypeCell) {
    _prototypeCell = [self.tabView
     dequeueReusableCellWithIdentifier:@"rwCell"];
  }
  return _prototypeCell;
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }

    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);

    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];

    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    //return image
    return image;
}

#pragma mark - API
- (void) getReviewersFromServerWithOffset:(NSInteger)offset
                                  andDate:(NSString *)rwDate
                              andRewiewer:(NSString *)reviewer
                                 andQuery:(NSString *)query{
    [[ServiceManager sharedManager] getRevieweresWothOffset:offset//[self.revieweres count]
                                                   andQuery:query
                                                    andDate:rwDate
                                                andRewiewer:reviewer
                                                  onSuccess:^(NSDictionary* _Nonnull reviewes) {
    
        self.isCanLoad = [reviewes objectForKey:@"has_more"];
        NSArray* resp = [reviewes objectForKey:@"results"];
        
        self.reviewes = [[self.reviewes mutableCopy] arrayByAddingObjectsFromArray:resp];
        [self.tabView reloadData];
    }
                                                  onFailure:^(NSError* _Nonnull error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %d",error.localizedDescription, (int)statusCode);
    }];
}
#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.reviewes count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identifier = @"rwCell";
    ReviewersTableViewCell *cell = (ReviewersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
   
    NSDictionary* rev = [self.reviewes objectAtIndex:indexPath.row];
    cell.filmTitle.text = [NSString stringWithFormat:@"%@",[rev objectForKey:@"display_title"]];
    cell.rwDescription.text = [NSString stringWithFormat:@"%@",[rev objectForKey:@"summary_short"]];
    cell.byline.text = [NSString stringWithFormat:@"%@",[rev objectForKey:@"byline"]];
    cell.recDate.text = [self ReturnFormattedDateFromString:[NSString stringWithFormat:@"%@",[rev objectForKey:@"date_updated"]]];
    NSDictionary* multimedia = [rev objectForKey:@"multimedia"];
    if (![multimedia isKindOfClass:[NSNull class]])
    {
        NSString * url_str = [NSString stringWithFormat:@"%@",[multimedia objectForKey:@"src"]];
        NSURL *url = [NSURL URLWithString:url_str];
          // @"byline"
         NSData *data = [NSData dataWithContentsOfURL:url];
        CGSize c=CGSizeMake(100, 100);
        cell.imgView.image = [self image:[UIImage imageWithData:data] scaledToSize:c] ;
    }
    //cell.imgView.image = [UIImage imageWithData:data];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    [self.prototypeCell layoutIfNeeded];
CGSize maximumLabelSize = CGSizeMake(310, 9999);
    CGRect titleRect = [self.prototypeCell.filmTitle.text boundingRectWithSize:maximumLabelSize
    options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    attributes:@{NSFontAttributeName:self.prototypeCell.filmTitle.font}
    context:nil];
    CGRect dateRect = [self.prototypeCell.recDate.text boundingRectWithSize:maximumLabelSize
    options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    attributes:@{NSFontAttributeName:self.prototypeCell.recDate.font}
    context:nil];
    CGRect descRect = [self.prototypeCell.rwDescription.text boundingRectWithSize:maximumLabelSize
    options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
    attributes:@{NSFontAttributeName:self.prototypeCell.rwDescription.font}
    context:nil];
    CGRect byline = [self.prototypeCell.byline.text boundingRectWithSize:maximumLabelSize
       options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
       attributes:@{NSFontAttributeName:self.prototypeCell.byline.font}
       context:nil];
    return titleRect.size.height + descRect.size.height + dateRect.size.height + byline.size.height + 86;
   
}

-(NSString*)ReturnFormattedDateFromString:(NSString*)date{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    NSLog(@"Picked the date %@", [dateFormatter stringFromDate:dateFromString] )  ;
    return [dateFormatter stringFromDate:dateFromString];
}

- (void)configureCell:(UITableViewCell *)cell
  forRowAtIndexPath:(NSIndexPath *)indexPath {
  if ([cell isKindOfClass:[ReviewersTableViewCell class]])
  {
    ReviewersTableViewCell *textCell = (ReviewersTableViewCell *)cell;
    textCell.filmTitle.text = [NSString stringWithFormat:@"%@",[[self.reviewes
    objectAtIndex:indexPath.row] objectForKey:@"display_title"]];;
    
    textCell.filmTitle.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    textCell.rwDescription.text = [NSString stringWithFormat:@"%@",[[self.reviewes
    objectAtIndex:indexPath.row] objectForKey:@"summary_short"]];;
    textCell.rwDescription.font = [UIFont fontWithName:@"Helvetica" size:10.0];
    textCell.recDate.text = [NSString stringWithFormat:@"%@",[[self.reviewes
    objectAtIndex:indexPath.row] objectForKey:@"date_updated"]];;
    textCell.recDate.font = [UIFont fontWithName:@"Helvetica" size:12.0];
      textCell.byline.text = [NSString stringWithFormat:@"%@",[[self.reviewes
      objectAtIndex:indexPath.row] objectForKey:@"byline"]];;
      textCell.byline.font = [UIFont fontWithName:@"Helvetica" size:12.0];
      
  }
}


- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)viewWillLayoutSubviews
{
}
 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
