//
//  CriticsViewController.h
//  filmsCritics
//
//  Created by Марина Явкина on 13/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CriticsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate,UISearchResultsUpdating>
@property (weak, nonatomic) IBOutlet UICollectionView *criticCollectionView;

@end

NS_ASSUME_NONNULL_END
