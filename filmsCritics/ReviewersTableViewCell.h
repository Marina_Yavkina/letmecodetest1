//
//  ReviewersTableViewCell.h
//  filmsCritics
//
//  Created by Марина Явкина on 09/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReviewersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *filmTitle;
@property (weak, nonatomic) IBOutlet UILabel *rwDescription;
@property (weak, nonatomic) IBOutlet UILabel *recDate;
@property (weak, nonatomic) IBOutlet UILabel *byline;

@end

NS_ASSUME_NONNULL_END
