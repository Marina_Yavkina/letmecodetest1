//
//  DetailViewController.h
//  filmsCritics
//
//  Created by Марина Явкина on 13/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageCr;
@property (weak, nonatomic) IBOutlet UILabel *nameCr;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *descr;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tabView;
@property (strong, nonatomic) NSString * selectedName;
@property (strong, nonatomic) NSString * selectedStatus;
@property (strong, nonatomic) NSString * selectedBio;
@property (strong, nonatomic) UIImage * selectedImage;

@end

NS_ASSUME_NONNULL_END
