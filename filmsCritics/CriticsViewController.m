//
//  CriticsViewController.m
//  filmsCritics
//
//  Created by Марина Явкина on 13/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import "CriticsViewController.h"
#import "ServiceManager.h"
#import "CriticsViewCell.h"
#import "DetailViewController.h"

@interface CriticsViewController ()
@property (strong, nonatomic) NSArray* critics;
@property (strong, nonatomic) NSArray *filteredData;
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UISearchController *searchController;
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;
@end

#define PAGES 6;

@implementation CriticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.critics = [NSMutableArray array];
    self.filteredData = [NSMutableArray array];
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.flowLayout.minimumLineSpacing = 0;
    self.flowLayout.minimumInteritemSpacing = 0;
    [self.criticCollectionView setCollectionViewLayout:self.flowLayout animated:YES];
    //self.criticCollectionView.collectionViewLayout = self.flowLayout;
    self.criticCollectionView.showsHorizontalScrollIndicator = YES;
    self.criticCollectionView.pagingEnabled = YES;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchBar.delegate = self;
    _searchController.searchBar.showsCancelButton = YES;
    
    [self getCriticsFromServerWithQuery:@"all"];
    
   
    // Do any additional setup after loading the view.
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
// Search cancelled
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }

    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);

    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];

    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    //return image
    return image;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

     NSString *searchText = searchBar.text;
       if (searchText) {
           
           if (searchText.length != 0) {
               NSPredicate *predicate = [NSPredicate predicateWithFormat:@"display_name CONTAINS[cd] %@",searchText];
               self.filteredData = [self.critics filteredArrayUsingPredicate:predicate];
           }
           else {
               self.filteredData = self.critics;
           }
           
           [self.criticCollectionView reloadData];
           
       }

        
    
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}

#pragma mark - API
- (void) getCriticsFromServerWithQuery:(NSString *)query{
    [[ServiceManager sharedManager] getCriticsWithRequest:query
                                                onSuccess:^(NSDictionary* _Nonnull critics)
     {
        NSArray* resp = [critics objectForKey:@"results"];
        
        self.critics = [[self.critics mutableCopy] arrayByAddingObjectsFromArray:resp];
        self.filteredData = self.critics;
        [self.criticCollectionView reloadData];
     }
                                                onFailure:^(NSError* _Nonnull error, NSInteger statusCode) {
        NSLog(@"error = %@, code = %d",error.localizedDescription, (int)statusCode);
    }];
}

-(BOOL)UISearchController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    self.critics = nil;
    self.filteredData = nil;
    [self getCriticsFromServerWithQuery:searchString];
    
    return YES;
}

#pragma mark - UicollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.filteredData count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"criticCell";
    CriticsViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if ((self.filteredData) && ([self.filteredData count] != 0))
    {
    NSDictionary* ctr = [self.filteredData objectAtIndex:indexPath.row];
        cell.criticName.text = [NSString stringWithFormat:@"%@",[ctr objectForKey:@"display_name"]];
        NSDictionary* multimedia = [ctr objectForKey:@"multimedia"];
           if (![multimedia isKindOfClass:[NSNull class]])
           {
               NSDictionary* resource = [multimedia objectForKey:@"resource"];
               if (![resource isKindOfClass:[NSNull class]])
               {
                   NSString * url_str = [NSString stringWithFormat:@"%@",[resource objectForKey:@"src"]];
                   
                   NSURL *url = [NSURL URLWithString:url_str];
                     
                   NSError *error = [[NSError alloc] init];
                                  NSData *data = [NSData dataWithContentsOfURL:url options:0 error:&error];
                                 CGSize c = CGSizeMake(160, 160);
                                 cell.criticImage.image = [self image:[UIImage imageWithData:data] scaledToSize:c] ;
               }
              
           }
           else
           {
               UIImage *pict = [UIImage imageNamed:@"face"];
               cell.criticImage.image = pict;
           }
        
    }
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
 if ([segue.identifier isEqualToString:@"detailIdent"])
 {
     NSIndexPath *indPath = [[self.criticCollectionView indexPathsForSelectedItems] firstObject];
     DetailViewController * detailed = segue.destinationViewController;
     NSDictionary* ctr = [self.filteredData objectAtIndex:indPath.row];
      detailed.selectedName = [NSString stringWithFormat:@"%@",[ctr objectForKey:@"display_name"]];
      detailed.selectedBio =  [NSString stringWithFormat:@"%@",[ctr objectForKey:@"bio"]];
     detailed.selectedStatus = [NSString stringWithFormat:@"%@",[ctr objectForKey:@"status"]];
     NSDictionary* multimedia = [ctr objectForKey:@"multimedia"];
     if (![multimedia isKindOfClass:[NSNull class]])
     {
         NSDictionary* resource = [multimedia objectForKey:@"resource"];
         if (![resource isKindOfClass:[NSNull class]])
         {
             NSString * url_str = [NSString stringWithFormat:@"%@",[resource objectForKey:@"src"]];
             
             NSURL *url = [NSURL URLWithString:url_str];
             NSError *error = [[NSError alloc] init];
                            NSData *data = [NSData dataWithContentsOfURL:url options:0 error:&error];
                           CGSize c = CGSizeMake(160, 160);
             detailed.selectedImage = [self image:[UIImage imageWithData:data] scaledToSize:c] ;
         }
     }
     else
     {
         detailed.selectedImage = [UIImage imageNamed:@"face"];
     }
         
     
     NSLog(detailed.selectedName);
    
     
 }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(202, 202);
}

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
