//
//  SceneDelegate.h
//  filmsCritics
//
//  Created by Марина Явкина on 08/02/2020.
//  Copyright © 2020 Марина Явкина. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

